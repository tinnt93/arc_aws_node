import { Injectable } from '@nestjs/common';
import { UserDto } from '../dto/user.dto';
import { UserRepository } from '../repository/user.repository';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { UserEntity } from '../entity/user.entity';

@Injectable()
export class UserService {

    constructor(
        @InjectRepository(UserEntity)
        private userRepository: Repository<UserEntity>
    ) {}

    create(user: UserDto): Promise<UserDto>
    {
        return this.userRepository.save(user);
    }
}
