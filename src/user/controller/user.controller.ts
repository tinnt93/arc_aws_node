import { Controller, Get, Post, Body } from '@nestjs/common';
import { UserService }  from '../service/user.service';
import { UserDto } from 'src/user/dto/user.dto';

@Controller('users')
export class UserController {

    constructor(private userService: UserService){};

    @Post('/')
    create(@Body() user: UserDto): Promise<UserDto>
    {
        return this.userService.create(user);
    }
}
