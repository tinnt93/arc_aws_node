output "key_name" {
    description = "The ECS cluster name"
    value       = aws_key_pair.key.key_name
}
