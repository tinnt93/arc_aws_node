output "vpc_id" {
    description = "The VPC id"
    value       = var.is_exists_vpc ? data.aws_vpc.main[0].id : aws_vpc.main[0].id
}
output "public_subnet_ids" {
    description = "The id subnets public"
    value       = aws_subnet.public.*.id
}
output "private_subnet_ids" {
    description = "The id subnets private"
    value       = aws_subnet.private.*.id
}
output "vpc_cidr_block" {
    description = "The CIDR block for the VPC."
    value       = var.is_exists_vpc ? data.aws_vpc.main[0].cidr_block : aws_vpc.main[0].cidr_block
}