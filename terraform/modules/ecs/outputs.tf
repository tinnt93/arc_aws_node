output "ecs_cluster_name" {
    description = "The ECS cluster name"
    value       = aws_ecs_cluster.main.name
}
