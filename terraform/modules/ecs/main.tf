resource "aws_ecs_cluster" "main" {
    name = var.ecs_cluster_name
}

data "template_file" "app_template" {
    template = file("../modules/ecs/templates/ecs-task-definition.tpl")
    vars = {
        app_image      = var.app_image
        app_port       = var.app_port
        fargate_cpu    = var.fargate_cpu
        fargate_memory = var.fargate_memory
        aws_region     = var.aws_region
        aws_logs_group = var.cloudwatch_log_group_name
        container_name = var.container_name
    }
}

resource "aws_ecs_task_definition" "app" {
    count                    = var.is_build_ecs_service == true ? 1 : 0
    family                   = var.ecs_task_name
    execution_role_arn       = var.ecs_task_execution_role_arn
    network_mode             = "awsvpc"
    requires_compatibilities = ["FARGATE"]
    cpu                      = var.fargate_cpu
    memory                   = var.fargate_memory
    container_definitions    = data.template_file.app_template.rendered
}

resource "aws_ecs_service" "main" {
    count           = var.is_build_ecs_service == true ? 1 : 0
    name            = var.ecs_service_name
    cluster         = aws_ecs_cluster.main.id
    task_definition = aws_ecs_task_definition.app[count.index].arn
    desired_count   = var.number_containers
    launch_type     = "FARGATE"

    network_configuration {
        security_groups  = [var.ecs_task_security_group_id]
        subnets          = var.private_subnet_ids
        assign_public_ip = true
    }

    load_balancer {
        target_group_arn = var.alb_target_group_id
        container_name   = var.container_name
        container_port   = var.app_port
    }
}