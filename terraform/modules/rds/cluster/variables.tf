variable "identifier" {
    description = "The name of the RDS instance"
    default     = ""
}
variable "db_subnet_group_name" {
    description = "The name of the DB subnet group"
    default     = ""
}
variable "private_subnet_ids" {
    description = "A list of VPC subnet IDs"
    default     = []
}
variable "allocated_storage" {
    description = "The allocated storage in gibibytes"
    default     = ""
}
variable "engine" {
    description = "The database engine to use."
    default     = ""
}
variable "engine_version" {
    description = "(Optional) The engine version to use"
    default     = ""
}
variable "instance_class" {
    description = "The instance type of the RDS instance."
    default     = ""
}
variable "db_name" {
    description = "The database name."
    default     = ""
}
variable "username" {
    description = "The username."
    default     = ""
}
variable "password" {
    description = "The password."
    default     = ""
}
variable "database_authentication_enabled" {
    description = "Specifies whether or mappings of AWS Identity and Access Management (IAM) accounts to database accounts is enabled."
    default     = false
}
variable "rds_security_group_id" {
    description = "The RDS security group id."
    default     = []
}
variable "kms_key_id" {
    description = "The ARN for the KMS encryption key"
    default     = ""
}
variable "availability_zones" {
    description = "A list of EC2 Availability Zones for the DB cluster storage where DB cluster instances can be created"
    default     = ["ap-northeast-2a", "ap-northeast-2b", "ap-northeast-2c"]
}
variable "backup_retention_period" {
    description = "The days to retain backups for"
    default     = 1
}
variable "preferred_backup_window" {
    description = "The daily time range during which automated backups are created if automated backups are enabled using the BackupRetentionPeriod parameter.Time in UTC"
    default     = "17:40-01:00"
}
variable "skip_final_snapshot" {
    description = "Determines whether a final DB snapshot is created before the DB cluster is deleted"
    default     = true
}
variable "number_cluster_instance" {
    description = "The number cluster instances"
    default     = 1
}
variable "is_kms_key" {
    description = "You want to using kms key for RDS?"
    default     = true
}

