resource "aws_db_subnet_group" "default" {
    name        = var.db_subnet_group_name
    subnet_ids  = var.private_subnet_ids
}

resource "aws_db_instance" "default" {
    identifier                          = var.identifier
    allocated_storage                   = var.allocated_storage
    engine                              = var.engine
    engine_version                      = var.engine_version
    instance_class                      = var.instance_class
    name                                = var.db_name
    username                            = var.username
    password                            = var.password
    iam_database_authentication_enabled = var.database_authentication_enabled
    skip_final_snapshot                 = "true"
    storage_encrypted                   = "true"
    db_subnet_group_name                = aws_db_subnet_group.default.id
    vpc_security_group_ids              = [var.rds_security_group_id]
    kms_key_id                          = var.kms_key_id
}