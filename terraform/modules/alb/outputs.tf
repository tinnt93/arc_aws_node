output "alb_target_group_id" {
    description = "The alb target group id"
    value       = aws_alb_target_group.app.id
}