resource "aws_security_group" "elasticache" {
    name        = var.elasticache_security_group_name
    description = var.elasticache_security_group_description
    vpc_id      = var.vpc_id
    ingress {
        from_port       = var.elasticache_port
        to_port         = var.elasticache_port
        protocol        = "tcp"
        cidr_blocks     = [var.vpc_cidr_block]
    }
    egress {
        protocol    = "-1"
        from_port   = 0
        to_port     = 0
        cidr_blocks = ["0.0.0.0/0"]
    }
    tags = {
        Name = var.elasticache_security_group_name
    }
}