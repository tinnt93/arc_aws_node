# Variables specify the provider
variable "aws_region" {
    description = "The region AWS"
    default     = "ap-northeast-2"
}
variable "aws_access_key" {
    description = "The AWS access key"
    default     = ""
}
variable "aws_secret_key" {
    description = "The AWS secret key"
    default     = ""
}
variable "project_name" {
    description = "The Project name"
    default     = ""
}
variable "env" {
    description = "The environment"
    default     = ""
}
# End variables specify the provider

# Variables network
variable "vpc_cidr_block" {
    description = "The CIDR block for the VPC."
    default = "172.17.0.0/16"
}
variable "az_count" {
    description = "Number of AZs to cover in a given region"
    default     = 1
}
variable "vpc_id" {
    description = "The VPC ID"
    default = ""
}
variable "is_exists_vpc" {
    description = "The state for exists VPC"
    default = false
}
# End variables network

# Variables Security group 
variable "alb_security_group_description" {
    description = "The security group description."
    default     = ""
}
variable "app_port" {
    description = "Port exposed by the docker image to redirect traffic to"
    default     = 80
}
variable "ecs_tasks_security_group_description" {
    description = "The security group description."
    default     = ""
}
# Cert attach for ALB
variable "route53_zone_name" {
    description = "The Hosted Zone name of the desired Hosted Zone"
    default     = ""
}
variable "route53_private_zone" {
    description = "Used with name field to get a private Hosted Zone"
    default = false
}
variable "alb_certificate_domain_name" {
    description = "A domain name for which the certificate should be issued"
    default = ""
}
variable "alb_subject_alternative_names" {
    description = "Set of domains that should be SANs in the issued certificate. To remove all elements of a previously configured list"
    default = []
}

# For RDS
variable "rds_security_group_description" {
    description = "The security group description for rds."
    default     = ""
}
variable "rds_port" {
    description = "The port for rds."
    default     = 5432
}
# For ec2
variable "ec2_security_group_description" {
    description = "The description security group"
    default     = ""
}
variable "ec2_bastion_ssh_port" {
    description = "The ssh port."
    default     = 22
}
# End Variables Security group

# Variables for ALB
variable "alb_name" {
    description = "The name of the alb."
    default = ""
}
variable "alb_target_group_name" {
    description = "The target group name"
    default     = ""
}
variable "health_check_path" {
    description = "The path health check."
    default     = "/"
}
variable "is_ssl" {
    description = "Set ssl for alb listener"
    default     = true
}
# End Variables for ALB

# Variables for ECS
variable "app_image" {
    description = "The app image docker."
    default = ""
}

variable "fargate_cpu" {
    description = "The fargate cpu."
    default = ""
}

variable "fargate_memory" {
    description = "The fargate memory."
    default = ""
}

variable "ecs_cluster_name" {
    description = "The ecs cluster name."
    default = ""
}

variable "ecs_service_name" {
    description = "The ecs service name."
    default = ""
}

variable "number_containers" {
    description = "Number of docker containers to run"
    default     = 1
}

variable "container_name" {
    description = "The container name"
    default     = ""
}
variable "ecs_task_name" {
    description = "The task name for ecs."
    default = ""
}
variable "is_build_ecs_service" {
    description = "The task name for ecs."
    default = false
}
# End Variables for ECS

# Variables ECR
variable "image_tag_mutability" {
    description = "The tag mutability setting for the repository. Must be one of: MUTABLE or IMMUTABLE. Defaults to MUTABLE"
    default = "MUTABLE"
}
variable "scan_on_push" {
    description = "Configuration block that defines image scanning configuration for the repository"
    default = false
}
# End Variables ECR

# Variables RDS
variable "allocated_storage" {
    description = "The allocated storage in gibibytes"
    default     = ""
}
variable "engine" {
    description = "The database engine to use."
    default     = ""
}
variable "engine_version" {
    description = "(Optional) The engine version to use"
    default     = ""
}
variable "instance_class" {
    description = "The instance type of the RDS instance."
    default     = ""
}
variable "db_name" {
    description = "The database name."
    default     = ""
}
variable "username" {
    description = "The username."
    default     = ""
}
variable "password" {
    description = "The password."
    default     = ""
}
variable "database_authentication_enabled" {
    description = "Specifies whether or mappings of AWS Identity and Access Management (IAM) accounts to database accounts is enabled."
    default     = false
}
variable "rds_security_group_id" {
    description = "The RDS security group id."
    default     = ""
}
variable "kms_key_id" {
    description = "The ARN for the KMS encryption key"
    default     = ""
}
variable "availability_zones" {
    description = "A list of EC2 Availability Zones for the DB cluster storage where DB cluster instances can be created"
    default     = ["ap-northeast-2a", "ap-northeast-2b", "ap-northeast-2c"]
}
variable "backup_retention_period" {
    description = "The days to retain backups for"
    default     = 1
}
variable "preferred_backup_window" {
    description = "The daily time range during which automated backups are created if automated backups are enabled using the BackupRetentionPeriod parameter.Time in UTC"
    default     = "17:40-01:00"
}
variable "skip_final_snapshot" {
    description = "Determines whether a final DB snapshot is created before the DB cluster is deleted"
    default     = true
}
variable "number_cluster_instance" {
    description = "The number cluster instances"
    default     = 1
}
variable "is_kms_key" {
    description = "You want to using kms key for RDS?"
    default     = true
}

# End Variables RDS

# EC2 bastion
variable "instance_type" {
    description = "The type of instance to start. Updates to this field will trigger a stop/start of the EC2 instance."
    default     = ""
}
variable "ami" {
    description = "The AMI to use for the instance."
    default     = ""
}
variable "number_instances" {
    description = "The number instances."
    default     = 1
}
