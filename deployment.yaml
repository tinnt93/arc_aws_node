AWSTemplateFormatVersion: "2010-09-09"
Description: >
  Cloud Formation deploy configuration for symlink service.

Parameters:
  ClusterName:
    Type: String
  ServiceName:
    Type: String
  EcrName:
    Type: String
  CommitId:
    Type: String
  ContainerPort:
    Type: Number
    Default: 80
  DesiredCount:
    Type: Number
    Default: 1
  SecurityGroups:
    Type: List<String>
  Subnets:
    Type: List<AWS::EC2::Subnet::Id>
  HealthCheckPath:
    Type: String
  VpcId:
    Type: String
  ListenerRulePathPattern:
    Type: CommaDelimitedList
    Default: "*"
  LoadBalancerListenerArn:
    Type: String
  ListenerRulePriority:
    Type: Number
    Default: 15
  CpuTask:
    Type: Number
    Default: 512
  MemoryTask:
    Type: Number
    Default: 1024

Resources:
  CloudWatchLogsGroup:
    Type: AWS::Logs::LogGroup
    Properties:
      LogGroupName: !Ref AWS::StackName
      RetentionInDays: 365

  ECSTaskExecutionRole:
    Type: AWS::IAM::Role
    Properties:
      AssumeRolePolicyDocument:
        Version: 2012-10-17
        Statement:
          - Effect: Allow
            Principal:
              Service:
                - ecs-tasks.amazonaws.com
                - lambda.amazonaws.com
            Action: sts:AssumeRole
      Policies:
        - PolicyName: ecs-service
          PolicyDocument:
            Version: 2012-10-17
            Statement:
              - Effect: Allow
                Action:
                  - ecs:CreateCluster
                  - ecs:DeregisterContainerInstance
                  - ecs:DiscoverPollEndpoint
                  - ecs:Poll
                  - ecs:RegisterContainerInstance
                  - ecs:StartTelemetrySession
                  - ecs:Submit*
                  - logs:CreateLogStream
                  - logs:PutLogEvents
                  - ecr:*
                  - es:*
                  - lamda:*
                  - cloudwatch:*
                  - s3:*
                Resource: "*"
      RoleName: !Sub ${ServiceName}-ECSTaskExecutionRole-${AWS::Region}

  TaskDefinition:
    Type: AWS::ECS::TaskDefinition
    Properties:
      ContainerDefinitions:
        - Name: !Ref ServiceName
          Essential: true
          Image: !Sub ${AWS::AccountId}.dkr.ecr.${AWS::Region}.amazonaws.com/${EcrName}:${CommitId}
          LogConfiguration:
            LogDriver: awslogs
            Options:
              awslogs-region: !Ref AWS::Region
              awslogs-group: !Ref CloudWatchLogsGroup
              awslogs-stream-prefix: !Ref ServiceName
          PortMappings:
            - ContainerPort: !Ref ContainerPort
      Cpu: !Ref CpuTask
      ExecutionRoleArn: !Ref ECSTaskExecutionRole
      Family: !Ref ServiceName
      Memory: !Ref MemoryTask
      NetworkMode: awsvpc
      RequiresCompatibilities:
        - FARGATE
      TaskRoleArn: !Ref ECSTaskExecutionRole

  Service:
    Type: AWS::ECS::Service
    Properties:
      ServiceName: !Sub ${ServiceName}
      Cluster: !Ref ClusterName
      DesiredCount: !Ref DesiredCount
      LaunchType: FARGATE
      LoadBalancers:
        - ContainerName: !Ref ServiceName
          ContainerPort: !Ref ContainerPort
          TargetGroupArn: !Ref TargetGroup
      NetworkConfiguration:
        AwsvpcConfiguration:
          AssignPublicIp: ENABLED
          SecurityGroups: !Ref SecurityGroups
          Subnets: !Split [',', !Join [',', !Ref Subnets]]
      TaskDefinition: !Ref TaskDefinition

  TargetGroup:
    Type: AWS::ElasticLoadBalancingV2::TargetGroup
    Properties:
      HealthCheckIntervalSeconds: 30
      HealthCheckPath: !Ref HealthCheckPath
      HealthCheckProtocol: HTTP
      HealthCheckTimeoutSeconds: 5
      HealthyThresholdCount: 5
      Name: !Ref ServiceName
      Port: !Ref ContainerPort
      Protocol: HTTP
      TargetGroupAttributes:
        - Key: deregistration_delay.timeout_seconds
          Value: 30
      TargetType: ip
      UnhealthyThresholdCount: 5 # this value must be the same as the healthy threshold count
      VpcId: !Ref VpcId

  ListenerRule:
    Type: AWS::ElasticLoadBalancingV2::ListenerRule
    Properties:
      Actions:
        - TargetGroupArn: !Ref TargetGroup
          Type: forward
      Conditions:
        - Field: path-pattern
          Values: !Ref ListenerRulePathPattern
      ListenerArn: !Ref LoadBalancerListenerArn
      Priority: !Ref ListenerRulePriority
